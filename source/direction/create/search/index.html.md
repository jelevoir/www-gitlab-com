---
layout: markdown_page
title: "Category Vision - Search"
---

- TOC
{:toc}

## Search

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Code search is important for project discovery and day to day use of repositories so that it is possible to find function usage or references directly in the browser before opening an issue or while doing a code review. It shouldn't be a requirement to clone the project to search the source code.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=search)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=search)
- [Overall Vision](/direction/create/)

Please reach out to PM Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**In progress (ETA 11.11): [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)** - Elasticsearch powers code search but is not available on GitLab.com currently because the index is too large due to the scale of the GitLab instance. Making code search work is an important feature that could prevent customers from choosing GitLab.com over self hosting.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)
